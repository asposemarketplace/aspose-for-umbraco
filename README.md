For more details about project and its content please goto [Documentation](https://bitbucket.org/asposemarketplace/aspose-for-umbraco/wiki/Home).

# What is Umbraco? #

Umbraco is one of only a few open source web content management systems built on Microsoft's .NET technology stack. This CMS is no "out the box" solution. To the contrary, it's a content management system for .NET web developers. And while it's relatively straightforward to use, one must first deal with a steep learning curve.


### What is the use of Aspose .NET Products? ###

[Aspose](http://www.aspose.com) are file format experts and provide APIs and components for various file formats including MS Office, OpenOffice, PDF and Image formats. These APIs are available on a number of development platforms including .NET
 frameworks &ndash; the .NET frameworks starting from version 2.0 are supported. If you are a .NET developer, you can use Aspose’s native .NET APIs in your .NET applications to process various file formats in just a few lines of codes. All the Aspose
 APIs don’t have any dependency over any other engine. For example, you don’t need to have MS Office installed on the server to process MS Office files.
 
 